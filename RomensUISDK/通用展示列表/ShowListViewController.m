//
//  ShowListViewController.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/9/23.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "ShowListViewController.h"
#import "DetailListViewController.h"
#import "ShowListTableViewCell.h"

@interface ShowListViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *listView;
@end

@implementation ShowListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatListViewUI];
}
#pragma mark - UI
-(void)creatListViewUI {
    self.view.backgroundColor = HEXCOLOR(0xf0f0f0ff);
    [self.view addSubview:self.listView];
    NSDictionary *views = NSDictionaryOfVariableBindings(_listView);
    
    [self.view addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|[_listView]|"]];
    [self.view addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|-[_listView]-|"]];
}
#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    ShowListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[ShowListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.titleLab.text = @"这是标题";
    cell.detailLab.text = @"这是详情";
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailListViewController *detailVc = [[DetailListViewController alloc] init];
    [self.navigationController pushViewController:detailVc animated:YES];
}
#pragma mark - lazy
-(UITableView *)listView {
    if (!_listView) {
        _listView = [[UITableView alloc] init];
        _listView.delegate = self;
        _listView.dataSource = self;
        _listView.tableFooterView = [[UIView alloc] init];
        _listView.estimatedRowHeight = 100;
        _listView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _listView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
