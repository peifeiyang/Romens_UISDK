//
//  ShowListModel.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/9/25.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShowListModel : NSObject

@property (nonatomic, copy) NSString *guid;
@property (nonatomic, copy) NSString *nameStr;
@property (nonatomic, copy) NSString *detailStr;

@end
