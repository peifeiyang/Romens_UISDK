//
//  DetailListTableViewCell.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/9/25.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "DetailListTableViewCell.h"
@interface DetailListTableViewCell() {
    UILabel *_titleLab;
    UILabel *_detailLab;
}
@end
@implementation DetailListTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    }
    return self;
}
-(void)creatListCellUI {
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.detailLab];
    _titleLab = self.titleLab;
    _detailLab = self.detailLab;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_titleLab, _detailLab);
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-[_titleLab]-[_detailLab]-|"]];
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|-[_titleLab]-|"]];
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|-[_detailLab]-|"]];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
