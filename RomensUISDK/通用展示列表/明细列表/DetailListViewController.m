//
//  ListDetailViewController.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/9/25.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "DetailListViewController.h"
#import "DetailListTableViewCell.h"

@interface DetailListViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *detailListView;
@end

@implementation DetailListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatDetailListViewUI];
}
#pragma mark - UI
-(void)creatDetailListViewUI {
    self.view.backgroundColor = HEXCOLOR(0xf0f0f0ff);
    [self.view addSubview:self.detailListView];
    NSDictionary *views = NSDictionaryOfVariableBindings(_detailListView);
    
    [self.view addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|[_detailListView]|"]];
    [self.view addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|-[_detailListView]-|"]];
}
#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    DetailListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[DetailListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.titleLab.text = @"这是标题";
    cell.detailLab.text = @"这是详情";
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(UITableView *)detailListView {
    if (!_detailListView) {
        _detailListView = [[UITableView alloc] init];
        _detailListView.delegate = self;
        _detailListView.dataSource = self;
        _detailListView.tableFooterView = [[UIView alloc] init];
        _detailListView.estimatedRowHeight = 100;
        _detailListView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _detailListView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
