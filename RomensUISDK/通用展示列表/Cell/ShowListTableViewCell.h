//
//  ShowListTableViewCell.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/9/25.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowListTableViewCell : UITableViewCell
/**
 标题Label
 */
@property (nonatomic, strong) UILabel *titleLab;

/**
 详情Label
 */
@property (nonatomic, strong) UILabel *detailLab;

/**
 布局
 */
-(void)creatListCellUI;
@end
