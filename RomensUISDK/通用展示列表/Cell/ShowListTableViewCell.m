//
//  ShowListTableViewCell.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/9/25.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "ShowListTableViewCell.h"
@interface ShowListTableViewCell ()
@end
@implementation ShowListTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self  creatListCellUI];
    }
    return self;
}

-(void)creatListCellUI{
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.detailLab];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_titleLab, _detailLab);
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-[_titleLab]-|"]];
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-[_detailLab]-|"]];
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|-[_titleLab]-[_detailLab(_titleLab)]-|"]];
}
#pragma mark -lazy
-(UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = systemDefaultFont;
        _titleLab.textColor = [UIColor blackColor];
        _titleLab.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _titleLab;
}
-(UILabel *)detailLab {
    if (!_detailLab) {
        _detailLab = [[UILabel alloc] init];
        _detailLab.font = systemDefaultFont;
        _detailLab.textColor = [UIColor lightGrayColor];
        _detailLab.numberOfLines = 0;
        _detailLab.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _detailLab;
}
- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
