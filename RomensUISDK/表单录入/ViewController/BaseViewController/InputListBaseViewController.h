//
//  InputListBaseViewController.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/4/18.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputBaseViewModel.h"
#import "InputMainListView.h"
#import "InputToolHelperClass.h"
#import "InputFunctionMenuClass.h"

#define HINTOFF -100

typedef NS_ENUM (NSInteger, NavigationRightBarItemType) {
    NavigationRightBarItemNormal = 0, //默认按钮，有编辑、完成按钮
    NavigationRightBarItemNone,   //无按钮
    NavigationRightBarItemFunction, //含有功能菜单按钮
    NavigationRightBarItemOnlyFunction //只有功能菜单按钮
};

typedef void (^InputReturnDataBlock) (InputDataModel *);
typedef void (^InputReturnErrorBlock) (NSString *);

@interface InputListBaseViewController : BaseController

//返回数据Block
@property (nonatomic, copy) InputReturnDataBlock inputReturnDataBlock;
//返回错误Block, 不想显示错误 传空即可
@property (nonatomic, copy) InputReturnErrorBlock inputReturnErrorBlock;

//主页View
@property (nonatomic,strong) InputMainListView *mainView;
//业务逻辑
@property (nonatomic,strong) InputBaseViewModel *viewModel;
//导航栏右侧按钮Type
@property (nonatomic, assign) NavigationRightBarItemType navigaRightBarItemType;
//默认url(用于数据选择，如http://im.yiyao365.cn/yyzs/index.php)
@property (nonatomic, copy) NSString *defaultUrl;
//是否隐藏加载框（默认NO,不隐藏）
@property (nonatomic,assign) BOOL hiddenHud;
//功能菜单对象
@property (nonatomic, strong) InputFunctionMenuClass *functionMenu;

/*****************************处理录入模板数据接口**************************************/
/**
 请求数据方法。
 成功数据返回InputReturnDataBlock
 错误数据返回InputReturnErrorBlock
 */
-(void)fetchListData;

/**
 出错时，重新请求方法
 */
-(void)reloadRequest;

/**
 点击编辑方法
 */
-(void)editInput;

/**
 判断模板是否有修改

 @return 返回NO，不提示。
 */
-(BOOL)updateInputVaulesByLocal;

/**
 判断模板中是否有必填项

 @return NO,不判断
 */
-(BOOL)judgeNodesIsMustInput;

/**
 获取每条输入项

 @param nodes 某个模块数据
 @return 格式化为key-value
 */
-(NSMutableDictionary *)inputValuesWithNodes:(NSMutableArray *)nodes;

/*****************************保存录入模板接口**************************************/
/**
 保存方法Url，需拼上queryType。无默认url

 @return (如：http://im.yiyao365.cn/yyzs/index.php/handle)
 */
-(NSString *)setupSaveInputDataUrl;

/**
 保存时拼接外部参数

 @param sectionModel 每个模块数据
 @return 参数字典
 */
-(NSMutableDictionary *)spliceInputDoneParames:(InputSectionModel *)sectionModel;

/**
 保存方法，请求服务端
 */
-(void)updateInputNodesByServer;

/**
 保存成功，默认pop回前一页。

 @param dataJson 成功返回的JSON
 */
-(void)saveSuccess:(NSString *)dataJson;

/*****************************功能菜单接口**************************************/

/**
 请求功能菜单
 */
-(void)requestFunctionMenu;

/*****************************其他接口**************************************/

/**
 创建导航栏右侧按钮
 */
-(void)creatEditBtn;      //编辑按钮
-(void)creatInputDoneBtn; //完成按钮
/**
 显示加载窗体

 @param msgStr 要展示的内容
 */
-(void)showHudView:(NSString *)msgStr;

/**
 隐藏加载窗体
 */
-(void)hiddenHudView;

/**
 更新SectionModel中Nodes中的值

 @param sectionModel sectionModel
 @param dict 需更新的key和values
 */
-(void)updateTemplteNodesValue:(InputSectionModel *)sectionModel withKeyValues:(NSDictionary *)dict;

@end
