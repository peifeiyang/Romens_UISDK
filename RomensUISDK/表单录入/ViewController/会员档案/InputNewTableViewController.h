//
//  InputSecondTableViewController.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/8/2.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "InputERPTableViewController.h"

@interface InputNewTableViewController : InputERPTableViewController

/**
 数据
 */
@property (nonatomic,strong) InputSectionModel *sectionModel;
@end
