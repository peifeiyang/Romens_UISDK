//
//  InputViewController.h
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/14.
//
//  模板录入模块
//      获取所有的数据。
//  需要跳转的View:
//      1.输入页,包括数据普通文本和数字;
//      2.选择页,包括单选多选;
//      3.关联选择,如省市县选择;
//      4.WEB页;
//  另,从底部弹出Action
//      1.日期选择

#import "InputListBaseViewController.h"
//模板类型,请求操作按钮时使用
#define FUNC_FILE_TYPE @"FILE"
#define FUNC_APPROVA_TYPE @"APPROVE"

@interface InputERPTableViewController : InputListBaseViewController

/**
 请求功能菜单时的Type
 */
@property (nonatomic, copy) NSString *funcType;

-(void)checkEditAuth;
@end
