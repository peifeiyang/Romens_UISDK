//
//  InputERPFunctionViewController.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/8/27.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import "InputERPFunctionViewController.h"

@interface InputERPFunctionViewController ()

@end

@implementation InputERPFunctionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
#pragma mark - 请求数据
-(void)fetchListData {
    __weak typeof(self)weakSelf = self;
    [self Request:self.requestUrl queryType:self.requestFunc postData:self.parames success:^(id respone) {
        WebRequestData *data = (WebRequestData*)respone;
        if (!isEmptyString(data.Error) || isEmptyString(data.RequestData)) {
            weakSelf.inputReturnErrorBlock(isEmptyString(data.Error)? @"数据为空": data.Error);
            return;
        }
        id object = [ToolHelper JsonToDictionary:data.RequestData];
        InputDataModel *model = [InputToolHelperClass handleInputDataWithObj:object];
        if (model) {
            model.editInput = YES;
            weakSelf.inputReturnDataBlock(model);
        } else {
            weakSelf.inputReturnErrorBlock(@"数据格式解析失败");
        }
    }];
}
-(NSString *)setupSaveInputDataUrl {
    return self.requestUrl;
}

-(void)saveSuccess:(NSString *)dataJson {
    id dataObj = [ToolHelper JsonToDictionary:dataJson];
    if ([dataObj isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dataDict = (NSDictionary *)dataObj;
        if ([dataDict[@"DATA"] isEqualToString:@"0"]) {
            [self showHint:[NSString stringWithFormat:@"提交审核失败：%@", dataDict[@"MSG"]] yOffset:HINTOFF];
            return;
        } else {
            [self showHint:[ToolHelper isEmptyStr:dataDict[@"MSG"]]  yOffset:HINTOFF];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self createNewViewController];
            });
            
        }
    } else {
        [self showHint:@"提交审核失败，数据格式出错" yOffset:HINTOFF];
    }
}
-(void)createNewViewController {
    NSMutableArray *newVcs = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [newVcs removeLastObject];
    [newVcs removeLastObject];
    [self.navigationController setViewControllers:newVcs animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
