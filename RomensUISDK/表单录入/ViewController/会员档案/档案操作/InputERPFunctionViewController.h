//
//  InputERPFunctionViewController.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/8/27.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputListBaseViewController.h"

@interface InputERPFunctionViewController : InputListBaseViewController

/**
 请求地址
 */
@property (nonatomic, copy) NSString *requestUrl;

/**
 请求方法名
 */
@property (nonatomic, copy) NSString *requestFunc;

/**
 请求参数
 */
@property (nonatomic, strong) NSDictionary *parames;
@end
