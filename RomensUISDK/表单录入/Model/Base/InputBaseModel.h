//
//  InputBaseModel.h
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/14.
//
//

#import <Foundation/Foundation.h>

@interface InputBaseModel : NSObject

/**
 name / caption
 */
@property (nonatomic,copy) NSString *nameCaption;

/**
 code / key
 */
@property (nonatomic,copy) NSString *codeKey;

/**
 cell标识，用于创建cell
 */
@property (nonatomic,copy) NSString *cellIdentifier;
@end
