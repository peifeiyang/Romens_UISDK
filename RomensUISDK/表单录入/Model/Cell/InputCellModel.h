//
//  InputCellModel.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/7/31.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "InputBaseModel.h"
#import "RomensSDK/ActionSheetContentModel.h"

@interface InputCellModel : InputBaseModel

/**
 inputtype 用于区别跳转不同的界面 输入、选择View、PickView
 */
@property (nonatomic,copy) NSString *inputtype;

/**
 输入的内容
 */
@property (nonatomic,copy) NSString *inputDetail;

/**
 选择的内容(选择项编号)
 */
@property (nonatomic,copy) NSString *combinDetail;

/**
 是否是必填项,1为必填项
 */
@property (nonatomic,assign) BOOL isMustInput;

/**
 是否隐藏(1是隐藏)
 */
@property (nonatomic,assign) BOOL ishidden;

/**
 是否被锁定(判断是否允许编辑，1是不可以编辑)
 */
@property (nonatomic,assign) BOOL isLock;

/**
 标题是否高亮
 */
@property (nonatomic,assign) BOOL isHighlight;

/**
 格式化
 */
@property (nonatomic,copy) NSString *formatString;

/**
 格式化数据规则
 */
@property (nonatomic,copy) NSString *inputFormat;

/**
 公式
 */
@property (nonatomic,copy) NSString *formula;

/**
 引用列。1.用于公式计算关联项；2.用于搜索引用项（参数）
 */
@property (nonatomic,strong) NSArray *reference;

/**
 限制最小值
 */
@property (nonatomic,copy) NSString *minValue;

/**
 限制最大值
 */
@property (nonatomic,copy) NSString *maxValue;

/**
 最大录入长度
 */
@property (nonatomic,copy) NSString *maxLength;

/**
 最小录入长度
 */
@property (nonatomic,copy) NSString *minLength;

/**
 小数部分要保留的位数
 */
@property (nonatomic, strong) NSString *decimalPlace;

/**
 关系 (用于关联性选择)
 */
@property (nonatomic,copy) NSString *relation;

/**
 键盘类型 (仅适用于input类型的View)
 */
@property (nonatomic,assign) UIKeyboardType keyboardType;

/**
 用于选择View (适用于单选、多选类型的View)
 */
@property (nonatomic,strong) NSMutableArray<ActionSheetContentModel *> *items;

/**
 bool类型的数据 比如["否", "是"]
 */
@property (nonatomic,strong) NSArray *boolItems;

/**
 不能输入特殊文字的 正则表达式
 */
@property (nonatomic, copy) NSString *textSpecialExp;
/**
 输入特殊文字时的提示内容
 */
@property (nonatomic, copy) NSString *textSpecial;
/******************数据选择用到的参数*******************/
/**
 请求方法的Handle
 */
@property (nonatomic, copy) NSString *sourceHandle;

/**
 数据选择请求方法名
 */
@property (nonatomic, copy) NSString *sourceQueryType;

/**
 数据选择后需要刷新项
 */
@property (nonatomic, strong) NSArray *sourceCallBack;

@property (nonatomic, strong) NSArray *parames;

@property (nonatomic, copy) NSString *templateCode;

@property (nonatomic, copy) NSString *mainKey;
@end
