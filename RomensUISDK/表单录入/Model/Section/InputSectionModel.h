//
//  InputSectionModel.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/7/31.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "InputBaseModel.h"
#import "InputCellModel.h"

@interface InputSectionModel : InputBaseModel

/**
 cellModels
 */
@property (nonatomic,retain) NSMutableArray<InputCellModel *> *nodes;

/**
 ID字段，请求下一页的参数
 */
@property (nonatomic,copy) NSString *idString;

/**
 保存方法
 */
@property (nonatomic,copy) NSString *updateFunc;

/**
 保存方法的参数(带@的key从本地取)
 */
@property (nonatomic,copy) NSString *updateParams;

/**
 不带@的参数
 */
@property (nonatomic,strong) NSMutableArray *updateParamsArray;

/**
 保存上传数据的集合
 */
@property (nonatomic,strong) NSDictionary *dataDic;

/**
 关闭section (默认打开)
 */
@property(nonatomic,assign) BOOL close;
@end
