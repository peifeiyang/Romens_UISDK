//
//  InputDataModel.h
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/17.
//
//

#import "InputSectionModel.h"

@interface InputDataModel : NSObject

/**
 observe对象1
 */
@property (nonatomic,strong) NSMutableArray<InputSectionModel *> *templates;

/**
 observe对象2
 */
@property (nonatomic,strong) NSMutableArray<InputDataModel *> *dataModels;

/**
 类型
 */
@property (nonatomic,copy) NSString *modelType;

/**
 页名
 */
@property (nonatomic,copy) NSString *pageName;

/**
 node是否可编辑(是否可更改、填入信息)
 */
@property (nonatomic,assign) bool editInput;

@end
