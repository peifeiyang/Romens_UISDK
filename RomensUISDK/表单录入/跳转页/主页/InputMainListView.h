//
//  InputBaseView.h
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/14.
//
//  主页

#import <UIKit/UIKit.h>

#import "InputBaseViewModel.h"

@interface InputMainListView : UIView

/** VM **/
@property (nonatomic,strong)InputBaseViewModel *viewModel;

/** 列表数据 **/
@property (strong, nonatomic)NSMutableArray *listModelArray;
@end
