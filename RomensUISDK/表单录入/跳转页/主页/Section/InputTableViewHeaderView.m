//
//  InputHeaderView.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/7/31.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "InputTableViewHeaderView.h"
@interface InputTableViewHeaderView ()
@property (nonatomic,strong) UIView *leftView;
@end
@implementation InputTableViewHeaderView
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self creatTableViewHeaderViewUI];
    }
    return self;
}
-(void)creatTableViewHeaderViewUI{
    self.contentView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.leftView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.rightImageView];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_leftView, _titleLabel, _rightImageView);
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|[_leftView(5)]-[_titleLabel]"]];
     [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:[_rightImageView(30)]-|"]];
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|[_leftView]|"]];
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:[_rightImageView(30)]"]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_titleLabel
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant:0.0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_rightImageView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant:0.0]];
}
-(UIView *)leftView{
    if (!_leftView) {
        _leftView                                           = [[UIView alloc] init];
        _leftView.translatesAutoresizingMaskIntoConstraints = NO;
        _leftView.backgroundColor                           = SystemColor;
    }
    return _leftView;
}
-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel                                           = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.textColor                                 = [UIColor blackColor];
        _titleLabel.font                                      = systemDefaultFont;
    }
    return _titleLabel;
}
-(UIImageView *)rightImageView{
    if (!_rightImageView) {
        _rightImageView                                           = [[UIImageView alloc] init];
        _rightImageView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _rightImageView;
}
@end
