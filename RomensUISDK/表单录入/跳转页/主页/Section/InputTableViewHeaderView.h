//
//  InputHeaderView.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/7/31.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputTableViewHeaderView : UITableViewHeaderFooterView

/**
 title
 */
@property(nonatomic,strong) UILabel *titleLabel;

/**
 右侧图片
 */
@property(nonatomic,strong) UIImageView *rightImageView;
@end
