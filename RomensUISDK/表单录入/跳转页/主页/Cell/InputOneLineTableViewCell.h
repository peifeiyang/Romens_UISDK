//
//  inputChoseValueTableViewCell.h
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/18.

//cell 样式:
// ______________________________________
// |                                    |
// | title               value     image|
// |                                    |
// --------------------------------------
#import "InputMultiLineTableViewCell.h"

@interface InputOneLineTableViewCell : InputMultiLineTableViewCell

@end
