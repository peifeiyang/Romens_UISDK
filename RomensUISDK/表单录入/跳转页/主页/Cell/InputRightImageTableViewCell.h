//
//  InputValueTableViewCell.h
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/15.
//
//cell 样式:
// ______________________________________
// |                                    |
// | title                   UIimageView|
// |                                    |
// --------------------------------------

#import "InputBaseTableViewCell.h"

@interface InputRightImageTableViewCell : InputBaseTableViewCell

@property(nonatomic,strong) UIImageView *inputImage;

//隐藏Image,默认显示(NO)
@property(nonatomic,assign) bool hiddenImage;

@end
