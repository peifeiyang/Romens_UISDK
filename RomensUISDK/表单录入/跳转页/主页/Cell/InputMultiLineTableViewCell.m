//
//  InputValueTableViewCell.m
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/15.
//
//

#import "InputMultiLineTableViewCell.h"

@interface InputMultiLineTableViewCell (){
    UILabel *_pointIcon;
    UILabel *_titleLabel;
    UIImageView *_inputImage;
}
@end

@implementation InputMultiLineTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self  creatValueUI];
    }
    return self;
}
-(void)creatValueUI {
    [self.contentView addSubview:self.valueLabel];
    
    _pointIcon = self.pointIcon;
    _titleLabel = self.titleLabel;
    _inputImage = self.inputImage;
    NSDictionary *views = NSDictionaryOfVariableBindings(_pointIcon,_titleLabel,_valueLabel,_inputImage);
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:[_pointIcon]-[_valueLabel]-[_inputImage]"]];
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:[_titleLabel]-8-[_valueLabel]-8-|"]];
}

-(UILabel *)valueLabel{
    if (!_valueLabel) {
        _valueLabel = [[UILabel alloc] init];
        _valueLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _valueLabel.font = systemDetailFont;
        _valueLabel.textColor = [UIColor blackColor];
        _valueLabel.numberOfLines = 0;
    }
    return _valueLabel;
}
@end
