//
//  InputValueTableViewCell.h
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/15.

//cell 样式:
// ______________________________________
// | title                              |
// |                               image|
// | value                              |
// --------------------------------------

#import "InputRightImageTableViewCell.h"
@interface InputMultiLineTableViewCell : InputRightImageTableViewCell

@property (nonatomic,strong) UILabel *valueLabel;

@end
