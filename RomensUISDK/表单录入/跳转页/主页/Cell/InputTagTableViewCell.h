//
//  InputTagTableViewCell.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/8/10.
//  Copyright © 2017年 Romens. All rights reserved.
//
//cell 样式:
// ______________________________________
// |                                    |
// | title                              |
// |                                    |
// | Tag   Tag   Tag                    |
// |                                    |
// | Tag   Tag   Tag                    |
// --------------------------------------

#import "InputRightImageTableViewCell.h"
#import "RomensSDK/SKTagView.h"

@interface InputTagTableViewCell : InputRightImageTableViewCell

/**
 标签View
 */
@property  (nonatomic, strong)SKTagView * tagView;
@end
