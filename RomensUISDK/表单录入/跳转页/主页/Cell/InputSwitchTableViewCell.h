//
//  InputSwitchTableViewCell.h
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/19.
//
//cell 样式:
// ______________________________________
// |                                    |
// | title                        switch|
// |                                    |
// --------------------------------------

#import "InputBaseTableViewCell.h"
#import <RomensSDK/ZJSwitch.h>

@interface InputSwitchTableViewCell : InputBaseTableViewCell

@property (nonatomic, strong) ZJSwitch *switchView;
@end
