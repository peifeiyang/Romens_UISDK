//
//  inputChoseValueTableViewCell.m
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/18.
//
//

#import "InputOneLineTableViewCell.h"
@interface InputOneLineTableViewCell (){
    UILabel *_titleLabel;
    UIImageView *_inputImage;
    UILabel *_valueLabel;
}

@end
@implementation InputOneLineTableViewCell

-(void)creatValueUI{
    self.valueLabel.numberOfLines = 1;
    self.valueLabel.textAlignment = NSTextAlignmentRight;
    
    [self.contentView addSubview:self.valueLabel];
    
    _titleLabel = self.titleLabel;
    _inputImage = self.inputImage;
    _valueLabel = self.valueLabel;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_titleLabel,_valueLabel,_inputImage);
    [self.contentView addConstraints:
     [ToolHelper GetNSLayoutCont:views format:@"H:[_titleLabel]-5-[_valueLabel]-[_inputImage]"]];
    
    [self.contentView addConstraint:
     [NSLayoutConstraint constraintWithItem:_titleLabel
                                  attribute:NSLayoutAttributeCenterY
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:_inputImage
                                  attribute:NSLayoutAttributeCenterY
                                 multiplier:1.0
                                   constant:0.0]];
    [self.contentView addConstraint:
     [NSLayoutConstraint constraintWithItem:_valueLabel
                                  attribute:NSLayoutAttributeCenterY
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:_inputImage
                                  attribute:NSLayoutAttributeCenterY
                                 multiplier:1.0
                                   constant:0.0]];
}
@end
