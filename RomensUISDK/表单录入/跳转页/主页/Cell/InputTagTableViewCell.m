//
//  InputTagTableViewCell.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/8/10.
//  Copyright © 2017年 Romens. All rights reserved.
//

#define cellPadding 10.0
#import "InputTagTableViewCell.h"

@interface InputTagTableViewCell (){
    UILabel *_titleLabel;
    UIImageView *_inputImage;
}

@end
@implementation InputTagTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self  creatTagUI];
    }
    return self;
}

-(void)creatTagUI{
    [self.contentView addSubview:self.tagView];
    _titleLabel = self.titleLabel;
    _inputImage = self.inputImage;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_titleLabel, _tagView, _inputImage);
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-[_tagView]-[_inputImage]"]];
      [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:[_titleLabel]-[_tagView]-8-|"]];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(SKTagView *)tagView {
    if (!_tagView) {
        _tagView                         = [[SKTagView alloc]init];
        _tagView.preferredMaxLayoutWidth = SCREEN_WIDTH - 40 * 2;
        _tagView.backgroundColor         = [UIColor whiteColor];
        _tagView.lineSpacing             = 5.0;
        _tagView.interitemSpacing        = 5.0;
        _tagView.singleLine              = NO;
        _tagView.padding                 = UIEdgeInsetsMake(0, 5, 0, 5);
        _tagView.translatesAutoresizingMaskIntoConstraints = false;
        
        [self.contentView addSubview:_tagView];
    }
    return _tagView;
}
@end
