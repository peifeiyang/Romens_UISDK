//
//  InputSwitchTableViewCell.m
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/19.
//
//

#import "InputSwitchTableViewCell.h"


@interface InputSwitchTableViewCell (){
    UILabel *_titleLabel;
}
@end
@implementation InputSwitchTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self  creatUI];
    }
    return self;
}

-(void)creatUI{
    _titleLabel = self.titleLabel;
    [self.contentView addSubview:self.switchView];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_titleLabel,_switchView);
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:[_switchView(50)]-|"]];
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:[_switchView]"]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_titleLabel
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant:0.0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_switchView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant:0.0]];
}

-(ZJSwitch *)switchView{
    if (!_switchView) {
        _switchView = [[ZJSwitch alloc] init];
        _switchView.translatesAutoresizingMaskIntoConstraints = NO;
        _switchView.onTintColor = SystemColor;
//        _switchView.onText = @"";
//        _switchView.offText = @"";
    }
    return _switchView;
}

@end
