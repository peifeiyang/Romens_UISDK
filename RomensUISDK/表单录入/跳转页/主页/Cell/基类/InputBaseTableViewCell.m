//
//  InPutBaseTableViewCell.m
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/15.
//
//

#import "InputBaseTableViewCell.h"

@implementation InputBaseTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self  creatTitleUI];
    }
    return self;
}

-(void)creatTitleUI{
    
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.pointIcon];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_pointIcon, _titleLabel);
    
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-[_pointIcon(8)]-[_titleLabel]-10@250-|"]];
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|-8-[_titleLabel]"]];
    [self.contentView addConstraint:[ToolHelper getCenterConstraintWithItem:_pointIcon to:self.titleLabel attribute:NSLayoutAttributeCenterY]];
}

-(void)setShowPointIcon:(bool)showPointIcon {
    _showPointIcon = showPointIcon;
    self.pointIcon.hidden = !showPointIcon;
}

-(void)setTitleHighlight:(bool)titleHighlight {
    _titleHighlight = titleHighlight;
    if (titleHighlight) {
        self.titleLabel.textColor = SystemColor;
    } else {
        self.titleLabel.textColor = [UIColor lightGrayColor];
    }
}

-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor lightGrayColor];
        _titleLabel.font = systemDefaultFont;
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _titleLabel;
}
-(UILabel *)pointIcon {
    if (!_pointIcon) {
        _pointIcon = [[UILabel alloc] init];
        _pointIcon.translatesAutoresizingMaskIntoConstraints = NO;
        _pointIcon.textColor = [UIColor redColor];
        _pointIcon.font = systemDetailFont;
        _pointIcon.textAlignment = NSTextAlignmentCenter;
        _pointIcon.text = @"*";
        _pointIcon.hidden = YES;
    }
    return _pointIcon;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
