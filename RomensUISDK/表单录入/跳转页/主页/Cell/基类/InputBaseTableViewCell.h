//
//  InPutBaseTableViewCell.h
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/15.
//
//cell基类

#import <UIKit/UIKit.h>

@interface InputBaseTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *pointIcon;

@property(nonatomic,strong) UILabel *titleLabel;

@property (nonatomic, assign) bool showPointIcon;

@property (nonatomic, assign) bool titleHighlight;
@end
