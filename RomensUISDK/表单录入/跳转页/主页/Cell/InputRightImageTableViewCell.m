//
//  InputValueTableViewCell.m
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/15.
//
//

#import "InputRightImageTableViewCell.h"

@interface InputRightImageTableViewCell (){
    UILabel *_titleLabel;
    NSArray *_hiddenConstraints;
    NSArray *_showConstraints;
}

@end
@implementation InputRightImageTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.hiddenImage = NO;
        [self creatImageUI];
    }
    return self;
}

-(void)creatImageUI{
    [self.contentView addSubview:self.inputImage];
    _titleLabel = self.titleLabel;
    NSDictionary *views = NSDictionaryOfVariableBindings(_titleLabel,_inputImage);
    _showConstraints = [ToolHelper GetNSLayoutCont:views format:@"H:[_inputImage(18)]-|"];
    _hiddenConstraints = [ToolHelper GetNSLayoutCont:views format:@"H:[_inputImage(0)]|"];
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:[_inputImage(18)]"]];
    [self.contentView addConstraint :[NSLayoutConstraint constraintWithItem:_inputImage
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant:0.0]];
}
-(void)updateConstraints {
    [super updateConstraints];
    if (self.hiddenImage) {
        [self.contentView removeConstraints:_hiddenConstraints];
        [self.contentView addConstraints:_showConstraints];
    } else {
        [self.contentView removeConstraints:_showConstraints];
        [self.contentView addConstraints:_hiddenConstraints];
    }
    
}
-(UIImageView *)inputImage{
    if (!_inputImage) {
        _inputImage = [[UIImageView alloc] init];
        _inputImage.image = [[UIImage imageNamed:GetBundleImage(@"RomensUISDK.bundle/InputView",@"ic_edit")] imageWithColor:[UIColor lightGrayColor]];
        _inputImage.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _inputImage;
}

@end
