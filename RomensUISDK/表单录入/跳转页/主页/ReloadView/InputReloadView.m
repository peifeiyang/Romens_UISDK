//
//  InputReloadView.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/8/7.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "InputReloadView.h"

#define ERRORMESSAGE @"网络加载错误"
@implementation InputReloadView

-(instancetype)init{
    if (self = [super init]) {
        [self creatReloadViewUI];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatReloadViewUI];
    }
    return self;
}
-(void)creatReloadViewUI{
    [self addSubview:self.reloadLabel];
    [self addSubview:self.reloadBtn];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_reloadLabel, _reloadBtn);
    
    [self addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-8-[_reloadLabel]-8-|"]];
    [self addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|-8-[_reloadLabel(<=100)]-8-[_reloadBtn]-|"]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_reloadBtn
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:_reloadLabel
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0
                                                      constant:0.0]];
}

-(UILabel *)reloadLabel{
    if (!_reloadLabel) {
        _reloadLabel           = [[UILabel alloc] init];
        _reloadLabel.font      = systemFont(20);
        _reloadLabel.textColor = [UIColor lightGrayColor];
        _reloadLabel.text      = ERRORMESSAGE;
        _reloadLabel.numberOfLines = 0;
        _reloadLabel.textAlignment = NSTextAlignmentCenter;
        _reloadLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _reloadLabel;
}

-(UIButton *)reloadBtn{
    if (!_reloadBtn) {
        _reloadBtn = [[UIButton alloc] init];
        [_reloadBtn.titleLabel setFont:systemDefaultFont];
        [_reloadBtn setTitle:@"请点击重试" forState:UIControlStateNormal];
        [_reloadBtn setTitleColor:SystemColor forState:UIControlStateNormal];
        _reloadBtn.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _reloadBtn;
}

@end
