//
//  InputReloadView.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/8/7.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputReloadView : UIView

/**
 重新加载提示语
 */
@property (nonatomic,strong) UILabel *reloadLabel;

/**
 重新加载按钮
 */
@property (nonatomic,strong) UIButton *reloadBtn;
@end
