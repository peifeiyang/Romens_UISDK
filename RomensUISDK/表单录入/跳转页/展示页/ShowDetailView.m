//
//  ShowDetailView.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/9/29.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "ShowDetailView.h"
@interface ShowDetailView ()
@property (nonatomic, strong) UILabel *titleLb;
@property (nonatomic, strong) UILabel *valueLb;
@end

@implementation ShowDetailView
#pragma mark - init
-(instancetype)init {
    if (self = [super init]) {
        [self creatShowViewUI];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatShowViewUI];
    }
    return self;
}

#pragma mark - UI
-(void)creatShowViewUI {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.titleLb];
    [self addSubview:self.valueLb];
    NSDictionary *views = NSDictionaryOfVariableBindings(_titleLb, _valueLb);
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_valueLb
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0
                                                      constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_valueLb
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0
                                                      constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_titleLb
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0
                                                      constant:0.0]];
     [self addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-[_titleLb]-|"]];
    [self addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-[_valueLb]-|"]];
    [self addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:[_titleLb]-[_valueLb]"]];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [self addGestureRecognizer:tapGesture];
}

#pragma mark - 打开、关闭
-(void)showContactView {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    window.windowLevel = UIWindowLevelAlert;
    self.alpha = 1;
    [window addSubview:self];
}
-(void)dismissContactView {
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.alpha = 0;
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        window.windowLevel = UIWindowLevelNormal;
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
    }];
}
#pragma mark -Lazy
-(UILabel *)titleLb {
    if (!_titleLb) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.font = systemFont(21);
        _titleLb.textColor = [UIColor lightGrayColor];
        _titleLb.textAlignment = NSTextAlignmentCenter;
        _titleLb.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _titleLb;
}
-(UILabel *)valueLb {
    if (!_valueLb) {
        _valueLb = [[UILabel alloc] init];
        _valueLb.font = systemFont(23);
        _valueLb.numberOfLines = 0;
        _valueLb.textAlignment = NSTextAlignmentCenter;
        _valueLb.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _valueLb;
}
#pragma mark - Set
-(void)setTitleStr:(NSString *)titleStr {
    _titleStr = titleStr;
    self.titleLb.text =titleStr;
}
-(void)setValueStr:(NSString *)valueStr {
    _valueStr = valueStr;
    self.valueLb.text = valueStr;
//    updateConstrain = YES;
//    [self setNeedsUpdateConstraints];
//    [self updateConstraintsIfNeeded];
}
@end
