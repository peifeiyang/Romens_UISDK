//
//  ShowDetailView.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/9/29.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowDetailView : UIView

@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, copy) NSString *valueStr;
-(void)showContactView;
@end
