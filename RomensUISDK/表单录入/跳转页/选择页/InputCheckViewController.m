//
//  InputCheckViewController.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/4/10.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import "InputCheckViewController.h"
#import "InputCheckTableViewCell.h"
#import "RomensSDK/ActionSheetContentModel.h"

@interface InputCheckViewController ()<UITableViewDelegate,UITableViewDataSource> {
    InputCellModel *_model;
    int _cellCount;
}
@property (nonatomic,strong)UITableView *mainTableView; /*选项列表*/
@property (nonatomic,strong)NSMutableArray<ActionSheetContentModel *> *items; /*选项*/
@end

#define CellHeight 45.0

@implementation InputCheckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
#pragma mark - set
-(void)setModel:(InputCellModel *)model {
    [super setModel:model];
    self.items = [[NSMutableArray alloc] init];
    _cellCount = 0;
    for (ActionSheetContentModel *itemModel in model.items) {
        [self.items addObject:[itemModel mutabCopy]];
        _cellCount += 1;
    }
    [self creatCheckUI];
}

-(void)creatCheckUI {
    [self.mainBackGroundView addSubview:self.mainTableView];
    NSDictionary *views = NSDictionaryOfVariableBindings(_mainTableView);
    [self.mainBackGroundView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|[_mainTableView]|"]];
    [self.mainBackGroundView addConstraints:[ToolHelper GetNSLayoutCont:views format:[NSString stringWithFormat:@"V:|[_mainTableView(%f)]|", [self tableViewVConstraints]]]];
}
//计算TableView的高度
-(CGFloat)tableViewVConstraints {
    CGFloat itemHeight =  _cellCount * CellHeight;
    if (itemHeight >= CONTENT_HEIGHT - 65) {
        itemHeight = CONTENT_HEIGHT - 65;
    }
    return itemHeight;
}
-(void)inputDone {
    if (self.inputDoneBlock) {
        self.model.items = self.items;
        self.inputDoneBlock(@"");
    }
    [super inputDone];
}

#pragma mark - TableView datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CellHeight;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!self.items || self.items.count <= 0) {
        return 0;
    }
    return self.items.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.items || self.items.count <= 0) {
        return [[UITableViewCell alloc] init];
    }
    static NSString *identifier = @"cell";
    InputCheckTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[InputCheckTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ActionSheetContentModel *model = self.items[indexPath.row];
    cell.titleLabel.text = model.contentTitle;
    if (model.isCheck) {
        cell.checkImage.hidden = NO;
    } else {
        cell.checkImage.hidden = YES;
    }
    return cell;
}
#pragma mark - TableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ActionSheetContentModel *itemModel = self.items[indexPath.row];
    if ([self.model.inputtype isEqualToString:LOOKUP_TYPE]) {
        for (ActionSheetContentModel *temp in self.items) {
            if (temp == itemModel) {
                temp.isCheck = YES;
            } else {
                temp.isCheck = NO;
            }
        }
    } else {
        itemModel.isCheck = !itemModel.isCheck;
    }
    [tableView reloadData];
}

#pragma mark - lazy
-(UITableView *)mainTableView {
    if (!_mainTableView) {
        _mainTableView = [[UITableView alloc] init];
        _mainTableView.backgroundColor = HEXCOLOR(0xf0f0f0ff);
        _mainTableView.delegate = self;
        _mainTableView.dataSource = self;
        _mainTableView.tableFooterView = [[UIView alloc] init];
        _mainTableView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _mainTableView;
}

#pragma mark - 解决tableViewCell 右移问题
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
