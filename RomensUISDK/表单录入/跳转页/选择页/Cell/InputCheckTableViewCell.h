//
//  InputCheckTableViewCell.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/8/8.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputCheckTableViewCell : UITableViewCell

/**
 title
 */
@property (nonatomic,strong) UILabel *titleLabel;

/**
 选择
 */
@property (nonatomic,strong) UIImageView *checkImage;
@end
