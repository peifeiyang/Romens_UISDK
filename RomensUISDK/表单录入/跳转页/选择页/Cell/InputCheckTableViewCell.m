//
//  InputCheckTableViewCell.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/8/8.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "InputCheckTableViewCell.h"

@implementation InputCheckTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self  creatCheckCellUI];
    }
    return self;
}

-(void)creatCheckCellUI{
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.checkImage];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_titleLabel, _checkImage);
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-[_titleLabel]-[_checkImage(25)]-20-|"]];
    [self.contentView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:[_checkImage(25)]"]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_titleLabel
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant:0.0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_checkImage
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant:0.0]];
}
#pragma mark - lazy
-(UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = systemDefaultFont;
//        _titleLabel.textColor = [UIColor lightGrayColor];
        _titleLabel.numberOfLines = 2;
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _titleLabel;
}
-(UIImageView *)checkImage {
    if (!_checkImage) {
        _checkImage = [[UIImageView alloc] init];
        _checkImage.translatesAutoresizingMaskIntoConstraints = NO;
        _checkImage.image = [[UIImage imageNamed:GetBundleImage(@"RomensUISDK.bundle/InputView",@"ic_done_white")] imageWithColor:SystemColor];
    }
    return _checkImage;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
