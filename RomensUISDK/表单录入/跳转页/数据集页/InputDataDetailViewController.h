//
//  InputDataSetViewController.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/9/14.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputBaseViewController.h"

@interface InputDataDetailViewController : InputBaseViewController
//请求的URL
@property (nonatomic, copy) NSString *requestUrl;
//请求的Handler
@property (nonatomic, copy) NSString *requestHandler;
//请求的方法名
@property (nonatomic, copy) NSString *requestQueryType;
//请求的参数
@property (nonatomic, strong) NSMutableDictionary *requestParames;

@property (nonatomic, copy) NSString *templateCode;
//主键
@property (nonatomic, copy) NSString *mainKey;
@end
