//
//  InputDataDetailTVDelegate.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/10/22.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import "InputDataDetailTVDelegate.h"

@implementation InputDataDetailTVDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    __block SimpleBaseItem *item = [[SimpleBaseItem alloc] initItemShowContentWithSimpleTable:self.dataArray[indexPath.section] index:indexPath.row];
    __weak typeof(self)wSelf = self;
    //删除
    UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        wSelf.actionBlock(InputDataDetailTVActionDelete, item);
    }];
    //编辑
    UITableViewRowAction *editRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"编辑" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        wSelf.actionBlock(InputDataDetailTVActionEdit, item);
    }];
    editRowAction.backgroundColor = SystemColor;
    return @[deleteRowAction,editRowAction];
}
@end
