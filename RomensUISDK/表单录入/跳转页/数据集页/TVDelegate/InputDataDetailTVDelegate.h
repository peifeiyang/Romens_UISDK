//
//  InputDataDetailTVDelegate.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/10/22.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RomensSDK/SimpleDataTVDelegate.h>

typedef NS_ENUM (NSInteger, InputDataDetailTVActionsType) {
    InputDataDetailTVActionDelete = 0,   //删除
    InputDataDetailTVActionEdit,         //编辑
};

typedef void(^DidEditActionsBlock)(InputDataDetailTVActionsType actionType, SimpleBaseItem *item);

@interface InputDataDetailTVDelegate : SimpleDataTVDelegate

@property (nonatomic, copy) DidEditActionsBlock actionBlock;
@end
