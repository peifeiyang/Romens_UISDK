//
//  InputAddDataViewController.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/9/14.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputListBaseViewController.h"

#define DATA_SELECT_LIST_RELOAD @"DataSelectListReload"

@interface InputAddDataViewController : InputListBaseViewController
@property (nonatomic, copy) NSString *requestTemplateUrl;

@property (nonatomic, copy) NSString *templateCode;

/**
 保存时要传的值(TGUID)
 */
@property (nonatomic, copy) NSString *mainKey;

/**
 guid,新增时传空；编辑传点击的那条GUID
 */
@property (nonatomic, copy) NSString *guid;
@end
