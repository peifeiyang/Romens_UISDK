//
//  InputAddDataViewController.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/9/14.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import "InputAddDataViewController.h"

@interface InputAddDataViewController ()

@end

@implementation InputAddDataViewController

- (void)viewDidLoad {
    self.navigaRightBarItemType = NavigationRightBarItemFunction;
    [super viewDidLoad];
}
#pragma mark - 请求数据
-(void)fetchListData {
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] init];
    __weak typeof(self)wSelf = self;
    postData[@"TEMPCODE"] = [ToolHelper isEmptyStr:self.templateCode];
    postData[@"GUID"] = isEmptyString(self.guid)? self.mainKey : self.guid;
    [self Request:[ToolHelper isEmptyStr:self.requestTemplateUrl] queryType:@"getSelectTemp" postData:postData success:^(id respone) {
        WebRequestData *data = (WebRequestData*)respone;
        if (!isEmptyString(data.Error) || isEmptyString(data.RequestData)) {
            self.inputReturnErrorBlock(isEmptyString(data.Error)? @"数据为空": data.Error);
            return;
        }
        id object = [ToolHelper JsonToDictionary:data.RequestData];
        InputDataModel *model = [InputToolHelperClass handleInputDataWithObj:object];
        if (model) {
            model.editInput = YES;
            wSelf.inputReturnDataBlock(model);
        } else {
            wSelf.inputReturnErrorBlock(@"数据格式解析失败");
        }
    }];
}

-(NSString *)setupSaveInputDataUrl {
    return [ToolHelper isEmptyStr:self.requestTemplateUrl];
}

-(NSMutableDictionary *)spliceInputDoneParames:(InputSectionModel *)sectionModel {
    NSMutableDictionary *parames = [super spliceInputDoneParames:sectionModel];
    parames[@"TEMPCODE"] = [ToolHelper isEmptyStr:self.templateCode];
    parames[@"TGUID"] = self.mainKey;
    return parames;
}

-(void)saveSuccess:(NSString *)dataJson {
    [[NSNotificationCenter defaultCenter] postNotificationName:DATA_SELECT_LIST_RELOAD object:nil];
    [super saveSuccess:dataJson];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
