//
//  InputSearchViewController.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/7/3.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import "InputBaseViewController.h"
typedef void (^DidSelectSourceDataBlock) (NSDictionary *);

@interface InputSearchViewController : InputBaseViewController
//搜索用到的URL
@property (nonatomic, copy) NSString *requestUrlStr;
//搜索用到的方法名
@property (nonatomic, copy) NSString *queryType;
//搜索用到的参数
@property (nonatomic, strong) NSMutableDictionary *parames;
//选择后的Block
@property (nonatomic, copy) DidSelectSourceDataBlock selectBlock;
@end
