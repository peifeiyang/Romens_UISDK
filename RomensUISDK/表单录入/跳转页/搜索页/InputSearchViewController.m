//
//  InputSearchViewController.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/7/3.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import "InputSearchViewController.h"
#import <RomensSDK/SearchView.h>
#import <RomensSDK/SimpleDataTable+OperateCategory.h>
#import <RomensSDK/SimpleAttributeStringTool.h>
#import <RomensSDK/UITableView+ShowEmptyBodyData.h>

@interface InputSearchViewController ()<inputDelegate,UITableViewDelegate, UITableViewDataSource> {
    BOOL _beginSearch;
}
@property (nonatomic, strong) SearchView *searchView;
@property (nonatomic, strong) UITableView *mainTableView;

@property (nonatomic, strong) SimpleDataTable *dataTable;
@end

@implementation InputSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatInputSearchUI];
}
-(void)creatInputSearchUI {
    self.view.backgroundColor = [UIColor whiteColor];
    [self.mainBackGroundView addSubview:self.searchView];
    [self.mainBackGroundView addSubview:self.mainTableView];
    
    NSDictionary *views = @{@"searchView": self.searchView,@"mainTableView": self.mainTableView};
    [self.mainBackGroundView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|[searchView]|"]];
    [self.mainBackGroundView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|[mainTableView]|"]];
    [self.mainBackGroundView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|-10-[searchView(44)]-10-[mainTableView]-|"]];
}
-(void)creatNavigationBarBtn {}
- (void)inputResuleText:(NSString *)text {
    if (isEmptyString(self.queryType) || isEmptyString(self.requestUrlStr)) {
        [self showHint:@"搜索失败，原因：配置数据出错"];
        return;
    }
    if (isEmptyString(text)) {
        return;
    }
    _beginSearch  = YES;
    [self querySourceData:text];
}
-(void)clearButtonClicked {
    self.dataTable = [[SimpleDataTable alloc] init];
    _beginSearch = NO;
    [self.mainTableView reloadData];
}
-(void)querySourceData:(NSString *)textStr {
    self.parames[@"SEARCHTEXT"] = textStr;
    
    __weak typeof(self)wSelf = self;
    [self Request:self.requestUrlStr queryType:self.queryType postData:self.parames success:^(id respone) {
        WebRequestData *data = (WebRequestData *)respone;
        if (!isEmptyString(data.Error) || isEmptyString(data.RequestData)) {
            [wSelf showHint:isEmptyString(data.Error)? @"请求数据为空，请稍后再试": data.Error];
            return;
        }
        id responeObj = [ToolHelper JsonToDictionary:data.RequestData];
        if ([responeObj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDict = (NSDictionary *)responeObj;
            wSelf.dataTable = [[SimpleDataTable alloc] initWithKeyValues:dataDict[@"DATA"]];
            [SimpleAttributeStringTool handleCustomTemplateWithDataTable:self.dataTable WithBoldFontSize:16];
            [wSelf.mainTableView reloadData];
        } else {
            [wSelf showHint:@"请求数据格式出错，请稍后再试"];
        }
    } showHud:YES];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = [self.dataTable dataCount];
    if (_beginSearch) {
        [tableView tableViewDisplayWitMsg:nil Message:@"未搜索到可用数据" ifNecessaryForRowCount:count];
    } else {
        [tableView tableViewDisplayWitMsg:nil Message:@"" ifNecessaryForRowCount:count];
    }
    return count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.estimatedRowHeight = 100;
    return UITableViewAutomaticDimension;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.textLabel.numberOfLines = 0;
    }
    NSDictionary *rowDict = self.dataTable.Data[indexPath.row];
    cell.textLabel.attributedText = rowDict[LOCAL_SHOW_TEMPLATE];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    if (self.selectBlock) {
        self.selectBlock(self.dataTable.Data[indexPath.row]);
    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(NSMutableDictionary *)parames {
    if (!_parames) {
        _parames = [[NSMutableDictionary alloc] init];
    }
    return _parames;
}
-(SearchView *)searchView {
    if (!_searchView) {
        _searchView = [[SearchView alloc] init];
        _searchView.translatesAutoresizingMaskIntoConstraints = false;
        _searchView.linecolor = SystemColor;
        _searchView.buttonBGColor = SystemColor;
        _searchView.buttonTitleColor = [UIColor whiteColor];
        _searchView.searchFiled.clearButtonMode = UITextFieldViewModeAlways;
        _searchView.placeholderStr = @"请输入您要查询的内容";
        _searchView.delegate = self;
    }
    return _searchView;
}
-(UITableView *)mainTableView {
    if (!_mainTableView) {
        _mainTableView = [[UITableView alloc] init];
        _mainTableView.translatesAutoresizingMaskIntoConstraints = false;
        _mainTableView.delegate = self;
        _mainTableView.dataSource = self;
        _mainTableView.tableFooterView = [[UIView alloc] init];
    }
    return _mainTableView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
