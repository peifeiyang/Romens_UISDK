//
//  InputDateTimePickerViewController.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/7/11.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import "InputDateTimePickerViewController.h"
#import <PGDatePicker/PGDatePickManager.h>

@interface InputDateTimePickerViewController ()<PGDatePickerDelegate> {
    InputCellModel *_model;
    PGDatePickerMode _pickerMode;
    NSString *_dateFormatStr;
}
@property (nonatomic, strong) PGDatePicker *datePicker;

@end

@implementation InputDateTimePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createDatePickerUI];
}
-(void)createDatePickerUI {
    [self.mainBackGroundView addSubview:self.datePicker];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_datePicker);
    [self.mainBackGroundView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|[_datePicker]|"]];
    [self.mainBackGroundView addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|[_datePicker(200)]|"]];
}

-(void)inputDone {
    [self.datePicker tapSelectedHandler]; 
}

-(void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents {
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDate *date = [gregorian dateFromComponents:dateComponents];
    NSString * dateString = [self formatDateStringWithDate:date];
    if (self.inputDoneBlock) {
        self.inputDoneBlock(dateString);
    }
    [super inputDone];
}

-(void)setModel:(InputCellModel *)model {
    [super setModel:model];
    //日期
    if ([model.inputtype isEqualToString:PICKDATE_TYPE]) {
        _pickerMode = PGDatePickerModeDate;
        _dateFormatStr = [model.inputFormat isEqualToString:NotFormat]? @"yyyy年MM月dd日": model.inputFormat;
    //日期时间
    } else if ([model.inputtype isEqualToString:DATETIME_TYPE]) {
        _pickerMode = PGDatePickerModeDateHourMinute;
        _dateFormatStr = [model.inputFormat isEqualToString:NotFormat]? @"yyyy年MM月dd日 HH:mm": model.inputFormat;
    }
    self.datePicker.datePickerMode = _pickerMode;

    //最大值，最小值
    if (!isEmptyString(model.maxValue)) {
        self.datePicker.maximumDate = [self formatDateWithString:model.maxValue];
    }
    if (!isEmptyString(model.minValue)) {
        self.datePicker.minimumDate = [self formatDateWithString:model.minValue];
    }
}
#pragma mark ---字符串转NSDate
- (NSDate *)formatDateWithString:(NSString *)dateStr {
    if (isEmptyString(dateStr)) {
        return nil;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:_dateFormatStr];
    NSDate *dafaultDate = [dateFormatter dateFromString:dateStr];
    return dafaultDate;
}
#pragma mark ---字符串转NSDate
-(NSString *)formatDateStringWithDate:(NSDate *)date {
    if (!date) {
        return  @"";
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = _dateFormatStr;
    NSString *dateString = [formatter stringFromDate:date];
    return [ToolHelper isEmptyStr:dateString];
}
-(PGDatePicker *)datePicker {
    if (!_datePicker) {
        _datePicker = [[PGDatePicker alloc] init];
        _datePicker.translatesAutoresizingMaskIntoConstraints = NO;
        _datePicker.textFontOfOtherRow = systemDefaultFont;
        _datePicker.isHiddenMiddleText = YES;
        _datePicker.datePickerType = PGPickerViewLineTypeline;
        _datePicker.delegate = self;
    }
    return _datePicker;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
