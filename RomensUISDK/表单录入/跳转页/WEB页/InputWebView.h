//
//  InputWebView.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/8/21.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputWebView : UIView
@property (nonatomic, copy) NSString *urlString;
@end
