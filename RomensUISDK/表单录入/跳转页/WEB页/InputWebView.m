//
//  InputWebView.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/8/21.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import "InputWebView.h"
#import <WebKit/WebKit.h>

@interface InputWebView() <WKScriptMessageHandler, WKNavigationDelegate>

@property (nonatomic, strong) WKWebView    *webView;      /*web页*/
@property(nonatomic,strong) UIProgressView *progressView; /*进度条*/
@end

@implementation InputWebView
#pragma mark - init
-(instancetype)init {
    if (self = [super init]) {
        [self creatWebView];
        [self observerProgress];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self creatWebView];
        [self observerProgress];
    }
    return self;
}
-(void)dealloc {
    //移除监听
    [self.webView removeObserver:self forKeyPath:@"loading"];
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
}
#pragma mark - UI
-(void)creatWebView {
    [self addSubview:self.webView];
    [self addSubview:self.progressView];
}
-(void)observerProgress {
    [self.webView addObserver:self
                   forKeyPath:@"loading"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView addObserver:self
                   forKeyPath:@"estimatedProgress"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
}
#pragma mark - KVO监听函数
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"estimatedProgress"]){
        self.progressView.progress = self.webView.estimatedProgress;
    }
    //隐藏progressView 须监听@"loading"
    if (!self.webView.loading) {
        [UIView animateWithDuration:0.5 animations:^{
            self.progressView.alpha = 0;
        }];
    }
}
#pragma mark -
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    NSLog(@"````````%@", message);
}
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    NSLog(@"开始加载");
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    
    NSLog(@"内容开始");
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    NSLog(@"页面加载完成");
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation {
    NSLog(@"加载失败");
}
#pragma mark - lazy
-(UIProgressView *)progressView {
    if (!_progressView) {
        CGRect rect = CGRectMake(0, 0, FULL_WIDTH, 10);
        _progressView = [[UIProgressView alloc] initWithFrame:rect];
        _progressView.backgroundColor = [UIColor redColor];
    }
    return _progressView;
}
-(WKWebView *)webView {
    if (!_webView) {
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        //初始化偏好设置属性：preferences
        config.preferences = [[WKPreferences alloc] init];
        //The minimum font size in points default is 0;
        config.preferences.minimumFontSize = 10;
        //是否支持JavaScript
        config.preferences.javaScriptEnabled = YES;
        //不通过用户交互，是否可以打开窗口
        config.preferences.javaScriptCanOpenWindowsAutomatically = NO;
        //通过JS与webView内容交互
        config.userContentController = [[WKUserContentController alloc] init];
        // 注入JS对象名称senderModel，当JS通过senderModel来调用时，我们可以在WKScriptMessageHandler代理中接收到
        [config.userContentController addScriptMessageHandler:self name:@"senderModel"];
        _webView = [[WKWebView alloc] initWithFrame:self.bounds configuration:config];
        _webView.navigationDelegate = self;
    }
    return _webView;
}

#pragma mark -SET
-(void)setUrlString:(NSString *)urlString {
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *quest = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:quest];
}
@end
