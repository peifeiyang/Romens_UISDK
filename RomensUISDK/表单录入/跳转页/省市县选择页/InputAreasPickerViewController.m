//
//  InputAreasPickerViewController.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/12/24.
//  Copyright © 2018 Romens. All rights reserved.
//

#import "InputAreasPickerViewController.h"
#import <PGPickerView/PGPickerView.h>
#import <Masonry/Masonry.h>

@interface InputAreasPickerViewController () <PGPickerViewDelegate, PGPickerViewDataSource> {
    InputCellModel *_model;
    bool _isDidLayoutSubviews;
}
@property (nonatomic, strong) PGPickerView *pickerView;

@property (nonatomic, strong) NSArray *areas;       //总数据源
@property (nonatomic, strong) NSArray *province;    //省
@property (nonatomic, strong) NSArray *city;        //市
@property (nonatomic, strong) NSArray *district;    //区
@end

@implementation InputAreasPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createAreasPickerUI];
    [self readArearsPlishFile];
}

- (void)createAreasPickerUI {
    [self.mainBackGroundView addSubview:self.pickerView];
   
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.mainBackGroundView);
        make.height.mas_equalTo(200);
    }];
}
-(void)inputDone {
    if (self.inputDoneBlock) {
        self.inputDoneBlock([self didConfirmed]);
    }
    [super inputDone];
}
#pragma mark 确定
- (NSString *)didConfirmed {
    NSInteger provinceIndex = [self.pickerView selectedRowInComponent:0];
    NSInteger cityIndex = [self.pickerView selectedRowInComponent:1];
    NSInteger districtIndex = [self.pickerView selectedRowInComponent:2];
    
    NSString *provinceStr = [_province objectAtIndex: provinceIndex];
    NSString *cityStr = [_city objectAtIndex: cityIndex];
    NSString *districtStr = [_district objectAtIndex:districtIndex];
    
    return [NSString stringWithFormat:@"%@/%@/%@",provinceStr,cityStr,districtStr];
}
#pragma 读取数据源
- (void)readArearsPlishFile {
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"areas" ofType:@"plist"];
    NSDictionary *rootDict = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    if (!rootDict[@"areas"]) {
        return;
    }
    self.areas = rootDict[@"areas"];
    [self selectedProvinceIndex:0 cityIndex:0];
}

#pragma mark 获取对应的省市区数据
- (void)selectedProvinceIndex:(NSInteger)provinceIndex cityIndex:(NSInteger)cityIndex
{
    //取出省
    NSMutableArray *provinceTmp = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.areas.count; i ++) {
        NSDictionary *areaTmpDict = self.areas[i];
        [provinceTmp addObject:[ToolHelper isEmptyStr:areaTmpDict[@"label"]]];
    }
    
    self.province = [NSArray arrayWithArray:provinceTmp];
    
    //取出市
    if (self.areas.count <= provinceIndex) {
        return;
    }
    NSDictionary *dicDict = self.areas[provinceIndex];
    NSArray *cityArray = dicDict[@"children"];
    
    NSMutableArray *cityTmp = [[NSMutableArray alloc] init];
    for (int i = 0; i < cityArray.count; i ++) {
        NSDictionary *cityTmpDict = cityArray[i];
        [cityTmp addObject:[ToolHelper isEmptyStr:cityTmpDict[@"label"]]];
    }
    
    self.city = [NSArray arrayWithArray:cityTmp];
    
    //取出区
    if (cityArray.count <= cityIndex) {
        return;
    }
    NSDictionary *districtDic = cityArray[cityIndex];;
    NSArray *districtArray = districtDic[@"children"];

    NSMutableArray *districtTmp = [[NSMutableArray alloc] init];
    for (int i = 0; i < districtArray.count; i ++) {
        NSDictionary *districtTomDict = districtArray[i];
        [districtTmp addObject:[ToolHelper isEmptyStr:districtTomDict[@"label"]]];
    }
    self.district = [NSArray arrayWithArray:districtTmp];
}
#pragma UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(PGPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(PGPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return self.province.count;
        
    } else if (component == 1) {
        return self.city.count;
        
    } else {
        return self.district.count;
    }
}
#pragma UIPickerViewDelegate
- (nullable NSString *)pickerView:(PGPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        return self.province[row];
        
    } else if (component == 1) {
        return self.city[row];
        
    } else {
        return self.district[row];
    }
}

- (void)pickerView:(PGPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        [self selectedProvinceIndex:row cityIndex:0];
        
        [self.pickerView selectedRowInComponent:1];
        [self.pickerView selectedRowInComponent:2];
        [self.pickerView reloadComponent:1];
        [self.pickerView reloadComponent:2];
        
    }else if (component == 1) {
        NSInteger provinceIndex = [self.pickerView selectedRowInComponent:0];
        [self selectedProvinceIndex:provinceIndex cityIndex:row];

        [self.pickerView selectedRowInComponent:2];
        [self.pickerView reloadComponent:2];
    }
}
- (PGPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[PGPickerView alloc] init];
        _pickerView.type = PGPickerViewLineTypeline;
        _pickerView.isHiddenMiddleText = false;
        _pickerView.textFontOfSelectedRow = systemDefaultFont;
        _pickerView.textFontOfOtherRow = systemDefaultFont;
        _pickerView.backgroundColor = [UIColor whiteColor];
        _pickerView.lineBackgroundColor = SystemColor;//设置线条的颜色
        _pickerView.textColorOfSelectedRow = SystemColor;//设置选中行的字体颜色
        _pickerView.textColorOfOtherRow = [UIColor lightGrayColor];//设置未选中行的字体颜色
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
    }
    return _pickerView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
