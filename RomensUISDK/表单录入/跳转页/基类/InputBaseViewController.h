//
//  InputBaseViewController.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/4/10.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputCellModel.h"

typedef void (^InputDoneBlock)(id inputDone);

@interface InputBaseViewController : BaseController

@property (nonatomic, strong) UIView *mainBackGroundView;
@property (nonatomic, strong) UILabel *msgLabel;
@property (nonatomic, strong) InputCellModel *model;

@property (nonatomic, copy) InputDoneBlock inputDoneBlock;
-(void)inputDone;
@end
