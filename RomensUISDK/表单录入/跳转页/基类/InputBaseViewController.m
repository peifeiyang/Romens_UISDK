//
//  InputBaseViewController.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/4/10.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import "InputBaseViewController.h"

@implementation InputBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatInputBaseUI];
    [self creatNavigationBarBtn];
}
- (void)creatInputBaseUI {
    self.view.backgroundColor = HEXCOLOR(0xf0f0f0ff);
}
- (void)creatMustInputUI {
    [self.view addSubview:self.mainBackGroundView];
    [self.view addSubview:self.msgLabel];

    NSDictionary *views = NSDictionaryOfVariableBindings(_mainBackGroundView, _msgLabel);
    [self.view addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-10-[_mainBackGroundView]-10-|"]];
    [self.view addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-10-[_msgLabel]-10-|"]];
    [self.view addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|-10-[_mainBackGroundView]-[_msgLabel]"]];
    [self.view addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:[_msgLabel]-10@250-|"]];
}
-(void)creatNotMustInputUI {
    [self.view addSubview:self.mainBackGroundView];
    NSDictionary *views = NSDictionaryOfVariableBindings(_mainBackGroundView);
    [self.view addConstraints:[ToolHelper GetNSLayoutCont:views format:@"H:|-10-[_mainBackGroundView]-10-|"]];
    [self.view addConstraints:[ToolHelper GetNSLayoutCont:views format:@"V:|-10-[_mainBackGroundView]-10@250-|"]];
}

-(void)creatNavigationBarBtn {
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:nil action:nil];
    self.navigationItem.backBarButtonItem = backBtnItem;
//    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:GetBundleImage(@"RomensUISDK.bundle/InputView",@"ic_done_white")] style:UIBarButtonItemStyleDone target:self action:@selector(inputDone)];
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(inputDone)];
    self.navigationItem.rightBarButtonItem = rightBtn;
}
-(void)inputDone {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}
-(void)setModel:(InputCellModel *)model {
    _model = model;
    self.title = model.nameCaption;
    //必选项，有提示
    if (model.isMustInput) {
        [self creatMustInputUI];
    } else {
        [self creatNotMustInputUI];
    }
}

-(UIView *)mainBackGroundView {
    if (!_mainBackGroundView) {
        _mainBackGroundView = [[UIView alloc] init];
        _mainBackGroundView.translatesAutoresizingMaskIntoConstraints = false;
    }
    return _mainBackGroundView;
}
-(UILabel *)msgLabel {
    if (!_msgLabel) {
        _msgLabel = [[UILabel alloc] init];
        _msgLabel.translatesAutoresizingMaskIntoConstraints = false;
        _msgLabel.font = systemDefaultFont;
        _msgLabel.textColor = [UIColor redColor];
        _msgLabel.text = @"*此项为必填项";
    }
    return _msgLabel;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
