//
//  InputFunctionMenuClass.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/7/23.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import "InputFunctionMenuClass.h"

@interface InputFunctionMenuClass()
@property (nonatomic, strong) NSArray *functionDataArray;
@end

@implementation InputFunctionMenuClass
-(instancetype)initWithFunctionData:(NSArray *)dataArray {
    if (self = [super init]) {
        self.functionDataArray = dataArray;
    }
    return self;
}

-(void)setFunctionDataArray:(NSArray *)functionDataArray {
    _functionDataArray = functionDataArray;
    NSMutableArray *models = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in functionDataArray) {
        InputFunctionModel *model = [[InputFunctionModel alloc] initWithDataDict:dict];
        [models addObject:model];
    }
    self.functionModels = [NSArray arrayWithArray:models];
}
@end
