//
//  InputFunctionMenuClass.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/7/23.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InputFunctionModel.h"

@interface InputFunctionMenuClass : NSObject

//功能菜单数据(处理后的数据)
@property (nonatomic, strong) NSArray <InputFunctionModel *>*functionModels;

/**
 构造函数

 @param dataArray 服务器数据
 @return 对象
 */
-(instancetype)initWithFunctionData:(NSArray *)dataArray;
@end
