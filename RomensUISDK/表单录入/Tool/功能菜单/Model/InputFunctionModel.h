//
//  InputFunctionModel.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/7/23.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InputFunctionModel : NSObject
//操作名字
@property (nonatomic, copy) NSString *name;
//操作描述
@property (nonatomic, copy) NSString *desc;
//头像
@property (nonatomic, copy) NSString *avatar;
//key
@property (nonatomic, copy) NSString *key;
//唯一标识
@property (nonatomic, copy) NSString *guid;
//类型
@property (nonatomic, copy) NSString *type;
//触发的方法
@property (nonatomic, copy) NSString *action;
//操作所需要的方法名
@property (nonatomic, copy) NSString *func;
//操作所需要的参数
@property (nonatomic, strong) NSDictionary *params;
//是否需要检测权限
@property (nonatomic, assign) bool checkAuth;
//检测时 需要的方法名
@property (nonatomic, copy) NSString *checkAuthfunc;

/**
 构造函数

 @param dict 服务器数据
 @return model对象
 */
-(instancetype)initWithDataDict:(NSDictionary *)dict;

/**
 获取参数值

 @param key 参数key
 @return 具体的值
 */
-(NSString *)selectParamesValue:(NSString *)key;
@end
