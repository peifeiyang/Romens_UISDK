//
//  InputFunctionModel.m
//  RomensUISDK
//
//  Created by 裴飞杨 on 2018/7/23.
//  Copyright © 2018年 Romens. All rights reserved.
//

#import "InputFunctionModel.h"
@interface InputFunctionModel () {
    NSDictionary *_paramesDict;
}
@end
@implementation InputFunctionModel

-(instancetype)initWithDataDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _paramesDict = dict;
        self.name = [ToolHelper isEmptyStr:dict[@"NAME"]];
        self.desc = [ToolHelper isEmptyStr:dict[@"DESC"]];
        self.avatar = [ToolHelper isEmptyStr:dict[@"AVATAR"]];
        self.key  = [ToolHelper isEmptyStr:dict[@"KEY"]];
        self.guid = [ToolHelper isEmptyStr:dict[@"GUID"]];
        self.type = [ToolHelper isEmptyStr:dict[@"TYPE"]];
        self.action = [ToolHelper isEmptyStr:dict[@"ACTION"]];
        self.func = [ToolHelper isEmptyStr:dict[@"FUNC"]];
        self.params = [self componentsParames:[ToolHelper isEmptyStr:dict[@"PARAMS"]]];

        NSDictionary *authDict = dict[@"AUTH"];
        if (authDict) {
            self.checkAuth = [[ToolHelper isEmptyStr:authDict[@"CHECK"]] boolValue];
            self.checkAuthfunc = [ToolHelper isEmptyStr:authDict[@"FUNC"]];
        }
    }
    return self;
}
-(NSDictionary *)componentsParames:(NSString *)parameStr {
    if (isEmptyString(parameStr)) {
        return [[NSDictionary alloc] init];
    }
    NSArray *parameKeys = [parameStr componentsSeparatedByString:@","];
    NSMutableDictionary *parames = [[NSMutableDictionary alloc] init];
    for (NSString *parameKey in parameKeys) {
        [parames setObject:[ToolHelper isEmptyStr:_paramesDict[parameKey]] forKey:parameKey];
    }
    return  [NSDictionary dictionaryWithDictionary:parames];
}
-(NSString *)selectParamesValue:(NSString *)key {
    if (isEmptyString(key)) {
        return @"";
    }
    return [ToolHelper isEmptyStr:_paramesDict[key]];
}
@end
