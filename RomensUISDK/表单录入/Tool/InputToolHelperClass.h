//
//  InputToolHelperClass.h
//  RomensUISDK
//
//  Created by 裴飞杨 on 2017/7/22.
//  Copyright © 2017年 Romens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InputDataModel.h"

typedef void (^SaveInputDataSuccessBlock) ();
typedef void (^SaveInputDataErrorBlock) (id);
@interface InputToolHelperClass : NSObject

/**
 解析会员档案的数据

 @param dataObj 请求的数据对象
 @return InputDataModel对象
 */
+(InputDataModel *)handleInputDataWithObj:(id)dataObj;

/**
 截取XX{XX}XX 中{XX}数据

 @param format 需要解析的String
 @return {XX}
 */
+(NSString *)handleFormatString:(NSString *)format;

/**
 根据规则来格式化数据

 @param type 类型
 @param detail 具体的数据
 @param format 格式化规则
 @return 格式化后的字符串
 */
+(NSString *)formatInputDetailWithInputType:(NSString *)type WithDetail:(NSString *)detail WithFormat:(NSString *)format;

/**
 获取该项的录入值

 @param model 某项Model
 @return 录入值(输入Type -> 输入值，选择Type -> 选择项)
 */
+(NSString *)selectNodeInputValue:(InputCellModel *)model;

/**
 根据Key获取对应的录入值

 @param nodeKey nodeKey
 @param sectionModel 模块
 @return 录入值
 */
+(NSString *)selectNodeInputValue:(NSString *)nodeKey byTemplate:(InputSectionModel *)sectionModel;

/**
 根据Key为Node赋值

 @param nodeKey nodeKey
 @param nodeValue 录入值
 @param sectionModel 模块
 */
+(void)setupNodeInputValue:(NSString *)nodeKey nodeValue:(NSString *)nodeValue byTemplate:(InputSectionModel *)sectionModel;
@end
