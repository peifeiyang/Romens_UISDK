
//
//  InputToolClass.h
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/17.
//
//

#ifndef InputToolClass_h
#define InputToolClass_h

//用户ID,姓名,手机号等必要的信息
#define INPUT_USER_ID    @"InputUserID"
#define INPUT_USER_NAME  @"InputUserName"
#define INPUT_USER_PHONE @"InputUserPhone"
#define INPUT_TEMPLATE_TYPE @"InputTemplateType" //录入模板类型

//格式化数据
#define NotFormat     @"notFormat"
#define NormalFormat  @"{VALUE}"
//获取默认值
#define CurrentDate    @"当前日期"
#define CurrentDateStr @""

//输入页、选择 类型
#define TEXT_MULTI_TYPE @"TEXT_MULTILINE"   //输入多行文本
#define TEXT_TYPE       @"TEXT"        //输入文本
#define INT_TYPE        @"INT"         //输入整数
#define DECIMAL_TYPE    @"DECIMAL"     //输入小数
#define MLOOKUP_TYPE    @"MLOOKUP"     //多项选择
#define LOOKUP_TYPE     @"LOOKUP"      //单项选择
#define PICKDATE_TYPE   @"DATE"        //日期选择
#define DATETIME_TYPE   @"DATETIME"    //日期时间选择
#define BOOL_TYPE       @"BOOL"        //bool
#define DATASOURCE_TYPE @"DATASOURCE"  //数据选择选择项,搜索选择
#define DATADETAIL_TYPE @"DATADETAIL"  //数据集，列表，可添加、删除
#define REGION_TYPE     @"REGION"      //省市区

//解析数据格式类型
#define DataModelType1  @"type1"
#define DataModelType2  @"type2"

//监听的key
#define ObserverKey     @"templates"

//cellIdentifier
#define MultiLineCellIdentifier     @"InputMultiLineTableViewCell"  //多行Cell
#define OneLineCellIdentifier       @"InputOneLineTableViewCell"    //单行Cell
#define SwitchCellIdentifier        @"InputSwitchTableViewCell"     //带UISWitch的Cell

//背景色
#define BackGroundGrayColor RGB(235, 235, 235)
//主题色
#define SystemColor RGBA(73, 134, 254, 1)//RGBA(72,156,221, 1)

#endif /* InputToolClass_h */
