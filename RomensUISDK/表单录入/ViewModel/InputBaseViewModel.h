//
//  InputBaseViewModel.h
//  RomensBI
//
//  Created by 裴飞杨 on 2017/7/14.
//
//

#import <Foundation/Foundation.h>

#import "InputDataModel.h"

typedef void (^ShowDetailViewBlock)(NSString *title, NSString *value);

@interface InputBaseViewModel : NSObject

@property (nonatomic,retain)InputDataModel *dataModel;
@property (nonatomic,copy)ShowDetailViewBlock showDetailViewBlock;

/**
 初始化

 @param rootVc 根ViewController
 @return VM实例
 */
-(instancetype)initWithRootViewController:(UIViewController*)rootVc;

/**
 跳转View
 @param indexpath index
 */
-(void)pushInputViewWithIndexPath:(NSIndexPath *)indexpath;

/**
 跳转新的输入ViewController

 @param section section
 */
-(void)pushNewInputViewControllerWithSection:(NSInteger)section;

/**
 监听UISwitch事件

 @param sender 控件
 */
-(void)switchAction:(id)sender;

/**
 点击编辑
 */
-(void)editInput;

/**
 判断必选项。不可编辑状态下 -> NO

 @return 有必选&&为空->YES，无必选||有必选不为空->NO
 */
-(BOOL)isMustInput;
/**
 展示具体value
 */
-(void)showDetailVauleWithCell:(UITableViewCell *)cell;
@end
